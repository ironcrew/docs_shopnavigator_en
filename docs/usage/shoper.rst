Shoper
======



Installation
^^^^^^^^^^^^



#. Log in to admin panel of your shop
#. Go to *Addons & integrations*
#. Find *Shopnavigator*: click *Select filters* > *Search* enter *Shopnavigator* and click *Filter*
#. Click installation button, when asked allow granting of priviledges (required to correct functioning of Shopnavigator)
#. If you do not have account at `<http://app.shopnavigator.net/>`_ then create it. Click *Sign Up* and provide username, email address and password
#. Log in `<http://app.shopnavigator.net/>`_
#. Go to application page of ShopNavigator in Shoper admin panel, provide username and password used in `<http://app.shopnavigator.net/>`_
#. Information about result of this action will be shown, if it failed try to do it using `<http://app.shopnavigator.net/>`_
#. Click *Link your account to Shoper shop* in *Integration Status*
#. Provide full address (starting with https) to main page of your shop and click *Apply*



Settings
^^^^^^^^



You can adjust Shopnavigator settings using `<http://app.shopnavigator.net/>`_.
Settings panel will appear after clicking *Settings* in *Integration Status*, lack of *Settings* means that installation is not finished.
Available settings are explained in table. After clicking *Apply* attempt of applying new settings will commence and information about result of action shown. Note that change requires some time.

+------------------------+------------------+-----------------------------------------------------------------------------+
| Name                   | Posible values   | Description                                                                 |
+------------------------+------------------+-----------------------------------------------------------------------------+
| Recommendations        | On/Off           | Turn on/off additional (built-in are left intact) products recommendations) |
+------------------------+------------------+-----------------------------------------------------------------------------+
| Search                 | On/Off           | Turn on/off search which replaces built-in Shoper search                    |
+------------------------+------------------+-----------------------------------------------------------------------------+
| Search highlight color | CSS color        | Color of background of product selected in Search                           |
+------------------------+------------------+-----------------------------------------------------------------------------+

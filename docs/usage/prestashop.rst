Prestashop
==========



ShopNavigator modules for Prestashop are destined for usage with version 1.7 or newer of Prestashop. Module *ShopNavigator search* provides search, whilst *ShopNavigator recommendations* provides products recommendations.



Installation - search
^^^^^^^^^^^^^^^^^^^^^



#. If you do not have account at `<http://app.shopnavigator.net/>`_ then create it. Click *Sign Up* and provide username, email address and password
#. Log in `<http://app.shopnavigator.net/>`_
#. Unveil token - click *view* near *Your API token* then save it 
#. Download *ShopNavigator search* `<http://app.shopnavigator.net/static/presta_shopnavigator_search.zip>`_
#. Log into admin panel of your shop
#. Go to *Modules* > *Module Manager*
#. Turn off built-in search: look for *searchbar* and turn it off (see screenshot)
#. Click *Load Module* and select file downloaded beforehand
#. After installation module configuration is required (see **Settings**) - without it module will not work

.. image:: /images/searchbaroff.png

Built-in search bar turning off.



Installation - recommendations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^



#. If you do not have account at `<http://app.shopnavigator.net/>`_ then create it. Click *Sign Up* and provide username, email address and password
#. Log in `<http://app.shopnavigator.net/>`_
#. Unveil token - click *view* near *Your API token* then save it 
#. Download *ShopNavigator recommendations* `<http://app.shopnavigator.net/static/presta_shopnavigator_reco.zip>`_
#. Log into admin panel of your shop
#. Go to *Modules* > *Module Manager*
#. Click *Load Module* and select file downloaded beforehand
#. After installation module configuration is required (see **Settings**) - without it module will not work



Settings
^^^^^^^^



Settings looks same for both modules

+----------------------------------------------------------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Name                                                                       | Explanation                                                  | Notes                                                                                                            |
+----------------------------------------------------------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Shopnavigator username                                                     | Username                                                     | Username as used in `<http://app.shopnavigator.net/>`_                                                           |
+----------------------------------------------------------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| API KEY                                                                    | User token                                                   | Available after log in to `<http://app.shopnavigator.net/>`_ as *Your API token*                                 |
+----------------------------------------------------------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Your shop hash                                                             | Id of your shop used by Shopnavigator                        | If left empty it will be generated based on your username, if you provide own it must have at least 6 characters |
+----------------------------------------------------------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Existing WebserviceKey with GET and HEAD access to products and categories | Key used for downloading data about products and categories  | Generated automatically during module installation                                                               |
+----------------------------------------------------------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+
| Shopnavigator url                                                          | ShopNavigator server address                                 | Address of ShopNavigator server where you have account                                                           |
+----------------------------------------------------------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------+

All fields but *Your shop hash* are compulsory (must not be left empty). Possible effects after clicking *Save*:

#. Settings updated - everything went as intended
#. Invalid Configuration value - missing or errorneous configuration value
#. Unable to add shop_hash to shopnavigator database - failure to add shop with given shop_hash, check API KEY (User token) correctness, if this does not help it is possible that somebody else is already using given shop hash, try changing *Your shop hash* value

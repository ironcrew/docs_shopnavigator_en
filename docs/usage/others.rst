Others
======



Installation - recommendations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^



Say you wish place widget with product suggestions with 3 cols, 2 rows for product with id 3505.

#. If you do not have account at `<http://app.shopnavigator.net/>`_ then create it. Click *Sign Up* and provide username, email address and password
#. Log in `<http://app.shopnavigator.net/>`_
#. Place following ``div`` in desired place: ``<div class="dd-recobox dd-col3 dd-row2"></div>``
#. Place following ``script``-s immediately before closing of body ``tag``: ``<script type="text/javascript"> window.ddt={baseUrl:'http://app.shopnavigator.net'}; window.onload=function(){ddt.getRecoBoxes('USERNAME', 3505);} </script> <script type="text/javascript" src="http://app.shopnavigator.net/scripts/sendData.js" async="true">`` where ``USERNAME`` is that used in 2nd step

If you wish you might specify product id for which suggestion are showed in ``div``'s class following way: ``<div class="dd-recobox dd-col3 dd-row2 dd-prod-3505"></div>``
Notes:

- function ``ddt.getRecoBoxes`` might be called with single argument - username
- if ``ddt.getRecoBoxes`` is called with single argument, all ``dd-recobox`` ``div``-s must have product id explicitly given in class — ``dd-recobox`` ``div``-s which do not fullfill this condition will not work at all
- specification in class has precedence over that from ``ddt.getRecoBoxes`` function argument
